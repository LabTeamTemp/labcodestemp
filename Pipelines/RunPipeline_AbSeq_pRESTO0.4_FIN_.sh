#!/bin/bash
# Super script to run all of the pipeline and make tables 
# Gets as an input a directory name
#third commit

FILEDIR=$1
cd $FILEDIR
FILEDIR=`basename $PWD`

FILEPARAM=$2
### SOURCE PARAMETERS
. "$FILEPARAM"

cat "$FILEPARAM" > $PARAMLOG

################# Filter Quality ################ 
echo "1: filterSeq filter"
$RUN  /home/userlab/presto-0.5.0/bin/FilterSeq.py quality -s $FILEPREFFIX"R1.fastq" -q $QUAL --nproc $NPROC --outname R1 > $RUNLOG
$RUN  /home/userlab/presto-0.5.0/bin/FilterSeq.py quality -s $FILEPREFFIX"R2.fastq" -q $QUAL --nproc $NPROC --outname R2 >> $RUNLOG

################# Mask Primers #################     
echo "2: maskPrimers score" 
$RUN   /home/userlab/presto-0.5.0/bin/MaskPrimers.py score -s R1_quality-pass.fastq -p $R1_PRIMER_FILE --mode cut --start $PR1ST --log MaskPrimers.R1.log --nproc $NPROC  --maxerror $MAXERR >> $RUNLOG
$RUN   /home/userlab/presto-0.5.0/bin/MaskPrimers.py score -s R2_quality-pass.fastq -p $R2_PRIMER_FILE --mode cut --start $PR2ST --barcode --log MaskPrimers.R2.log --nproc $NPROC --maxerror $MAXERR >> $RUNLOG # seems high.

################ Pair seqs and transfer barcodes between pairs #################
echo "3: PairSeq" 
$RUN  sh $PAIRAWK_BC R1_quality-pass_primers-pass.fastq R2_quality-pass_primers-pass.fastq

################# Gain some statistics on the primers and quality #################
echo "Quality:" > Count.log
echo "RAW>" $(awk 'NR%4==1{print $0}' R1.fastq | wc -l) >> Count.log
echo "READ1-QUALITY1>" $(grep -c -e "^\+$" R1_quality-pass.fastq) >> Count.log
echo "READ2-QUALITY1>" $(grep -c -e "^\+$" R2_quality-pass.fastq) >> Count.log
echo "Primers:" >> Count.log
echo "READ1-PRIMER2>" $(grep -c -e "^\+$"  R1_quality-pass_primers-pass.fastq) >> Count.log
echo "READ2-PRIMER2>" $(grep -c -e "^\+$"  R2_quality-pass_primers-pass.fastq) >> Count.log
echo "READ1-PAIRED3>" $(grep -c -e "^\+$"  R1_quality-pass_primers-pass_pair-pass.fastq) >> Count.log
echo "READ2-PAIRED3>" $(grep -c -e "^\+$"  R2_quality-pass_primers-pass_pair-pass.fastq) >> Count.log
$RUN   /home/userlab/presto-0.5.0/bin/ParseLog.py -l MaskPrimers.R2.log -f BARCODE PRIMER ERROR 
$RUN   /home/userlab/presto-0.5.0/bin/ParseLog.py -l MaskPrimers.R1.log -f PRIMER ERROR 

################ Align UID sets ################
echo "4: alignSets"     
$RUN   /home/userlab/presto-0.5.0/bin/AlignSets.py muscle -s R1_quality-pass_primers-pass_pair-pass.fastq --exec $MUSCLE --log AlignSets.R1.muscle.log  --nproc $NPROC >> $RUNLOG
$RUN   /home/userlab/presto-0.5.0/bin/AlignSets.py muscle -s R2_quality-pass_primers-pass_pair-pass.fastq --exec $MUSCLE --log AlignSets.R2.muscle.log  --nproc $NPROC >> $RUNLOG

############### Build Consensus #################
echo "5: BuildConsensus" 
#$RUN  BuildConsensus.py -s R1_quality-pass_primers-pass_pair-pass_align-pass.fastq --bf BARCODE --pf PRIMER --prcons $PRCONSR1 --log BuildConsensus.R1.log --nproc $NPROC -q 0 --maxdiv $MAXDIV --freq 0.6 >> $RUNLOG
#$RUN  BuildConsensus.py -s R2_quality-pass_primers-pass_pair-pass_align-pass.fastq --bf BARCODE --pf PRIMER --prcons $PRCONSR2 --log BuildConsensus.R2.log --nproc $NPROC -q 0 --maxdiv $MAXDIV --freq 0.6 >> $RUNLOG

#No maxdiv
$RUN  /home/userlab/presto-0.5.0/bin/BuildConsensus.py -s R1_quality-pass_primers-pass_pair-pass_align-pass.fastq --bf BARCODE --pf PRIMER --prcons $PRCONSR1 --log BuildConsensus.R1.log --nproc $NPROC -q 0 --maxerror 0.1 --freq 0.6 >> $RUNLOG
$RUN  /home/userlab/presto-0.5.0/bin/BuildConsensus.py -s R2_quality-pass_primers-pass_pair-pass_align-pass.fastq --bf BARCODE --pf PRIMER --prcons $PRCONSR2 --log BuildConsensus.R2.log --nproc $NPROC -q 0 --maxerror 0.1 --freq 0.6 >> $RUNLOG

################# Gain some statistics on the consensus #################
echo "Consensus:" >> Count.log
echo "READ1-SETS4>" $(grep "BARCODE>" -c BuildConsensus.R1.log) >> Count.log
echo "READ2-SETS4>" $(grep "BARCODE>" -c BuildConsensus.R2.log) >> Count.log
echo "READ1-CONSENSUS5>" $(grep "CONSCOUNT" -c R1_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass.fastq) >> Count.log
echo "READ2-CONSENSUS5>" $(grep "CONSCOUNT" -c R2_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass.fastq) >> Count.log
$RUN  /home/userlab/presto-0.5.0/bin/ParseLog.py -l BuildConsensus.R[12].log -f BARCODE SEQCOUNT CONSCOUNT PRIMER PRCOUNT PRFREQ

for f in *_primers-pass_pair-pass_align-pass_consensus-pass.fastq; do
    perl -i -pae 's/\|PRFREQ=[\d\.]+//; s/PRCONS=\K.*?(IG[GAMEDKL])\w*/$1/;' $f
done  

################# Pair sequences #################
echo "6: pairSequences" 
$RUN  sh $PAIRAWK_PRCONS R1_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass.fastq R2_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass.fastq

# $RUN  PairSeq.py -1 *R1*primers-pass_pair-pass_align-pass_consensus-pass.fastq -2 *R2*primers-pass_pair-pass_align-pass_consensus-pass.fastq 
echo "READ1-PAIRED6>" $(grep "CONSCOUNT" -c *R1*_primers-pass_pair-pass_align-pass_consensus-pass_pair-pass.fastq) >> Count.log
echo "READ2-PAIRED6>" $(grep "CONSCOUNT" -c *R2*_primers-pass_pair-pass_align-pass_consensus-pass_pair-pass.fastq) >> Count.log


#########Assemble sequences

echo "7.1: AssemblePairs" 
$RUN /home/userlab/presto-0.5.0/bin/AssemblePairs.py align \
    -1 R2_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass_pair-pass.fastq -2 R1_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass_pair-pass.fastq \
    --2f CONSCOUNT PRCONS --rc $REV \
    --minlen $MINLEN --alpha $ALPHA --maxerr $MAXERR_ASS --scanrev \
    --log AssemblePairs-align.log --nproc $NPROC --failed >> $RUNLOG

    
$RUN  /home/userlab/presto-0.5.0/bin/ParseLog.py -l AssemblePairs-align.log -f ID LENGTH OVERLAP ERROR PVALUE FIELDS1 FIELDS2 &


echo "ASSEMBLE ALIGN TOTAL AND PASS7:" >> Count.log
for f in *R2*pair-pass_assemble-pass.fastq *R2*pair-pass_assemble-fail.fastq ; do
    echo $f $(grep CONSCOUNT $f | \
        perl -nae '/(IG|TR)(\w)/; $s{$1.$2}++; END{ print "TOT: $."; map{print "\t$_ : $s{$_}"}sort keys %s} ' ) >> Count.log
done
        
# if you want to follow this up with AssemblePairs.py reference, do so here. 
echo "7.2: AssemblePairs with reference" 
$RUN /home/userlab/presto-0.5.0/bin/AssemblePairs.py reference \
    -1 R2_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass_pair-pass_assemble-fail.fastq -2 R1_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass_pair-pass_assemble-fail.fastq \
    --2f CONSCOUNT PRCONS --failed \
    --exec $USEARCH -r $REF_DB \
    --log AssemblePairs-reference.log --nproc $NPROC >> $RUNLOG
    
$RUN  /home/userlab/presto-0.5.0/bin/ParseLog.py -l AssemblePairs-reference.log -f ID REFID LENGTH OVERLAP GAP EVALUE1 EVALUE2 IDENTITY FIELDS1 FIELDS2 &

echo "ASSEMBLE REFERENCE TOTAL AND PASS7:" >> Count.log
for f in *R2*assemble-fail_assemble-pass.fastq; do
    echo $f $(grep CONSCOUNT $f | \
        perl -nae '/(IG|TR)(\w)/; $s{$1.$2}++; END{ print "TOT: $."; map{print "\t$_ : $s{$_}"}sort keys %s} ' ) >> Count.log
done

##### Gather stitched reads and reheader
$RUN  /home/userlab/presto-0.5.0/bin/ParseHeaders.py collapse -s *R2*_pair-pass_assemble-pass.fastq  *R2*assemble-fail_assemble-pass.fastq -f CONSCOUNT --act min 
cat *R2*_pair-pass_assemble-pass_reheader.fastq *R2*assemble-fail_assemble-pass_reheader.fastq >${FILEDIR}_Assembled_BCP.fastq

### BCP stands for - before Computational Primers ####

################ Process unstitched R1  #################

# Gather and rev comp all the R1 that did not stitch : might need some edits.
R1="R1_assemble-fail_rc"

$RUN  sh $PAIRAWK_ASS_FAIL R1_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass_pair-pass.fastq ${FILEDIR}_Assembled_BCP.fastq

seqtk seq -r R1_quality-pass_primers-pass_pair-pass_align-pass_consensus-pass_pair-pass_pair-fail.fastq > ${R1}_BCP.fastq
echo "Read1-NOTSTITCHED7>" $(grep -c -e '^\+$' $R1"_BCP.fastq" ) >> Count.log


################# Computational primers  - ONLY RELEVANT FOR IG SEQUENCES!!!

if [ $CHECK_COMP_PRIMERS=YES ] 
then
echo "8:  maskPrimers align (Computational primers)" 
$RUN /home/userlab/presto-0.5.0/bin/MaskPrimers.py align -s ${FILEDIR}_Assembled_BCP.fastq -p $MP_CREGION_PRIMERS --maxlen $MP_CREGION_MAXLEN --maxerror $MP_CREGION_MAXERR --mode tag --failed --nproc $NPROC  >> $RUNLOG
$RUN /home/userlab/presto-0.5.0/bin/MaskPrimers.py align -s ${R1}_BCP.fastq -p $MP_CREGION_PRIMERS --maxlen $MP_CREGION_MAXLEN --maxerror $MP_CREGION_MAXERR --mode tag --failed --nproc $NPROC >> $RUNLOG_R1

echo "READ12-COMP_PRIMER8>" $(grep -c '^\+$' ${FILEDIR}_Assembled_BCP_primers-pass.fastq) >> Count.log
echo "READ1-COMP_PRIMER8>" $(grep -c '^\+$' ${R1}_BCP_primers-pass.fastq) >> Count.log

mv ${FILEDIR}_Assembled_BCP_primers-pass.fastq ${FILEDIR}_Assembled.fastq
mv ${R1}_BCP_primers-pass.fastq ${R1}.fastq
else
echo "8:  maskPrimers align (Computational primers) - NO COMPUTATIONAL PRIMERS" 
mv ${FILEDIR}_Assembled_BCP.fastq ${FILEDIR}_Assembled.fastq
mv ${R1}_BCP.fastq ${R1}.fastq
fi

# Maskqual after assembled
################# Mask Quality ################ 
echo "9 FilterSeq maskqual"

$RUN  /home/userlab/presto-0.5.0/bin/FilterSeq.py maskqual -s ${FILEDIR}_Assembled.fastq -q $QUAL_MASK --nproc $NPROC  >> $RUNLOG

echo "ASSEMBLED-MASKQUALITY9>" $(grep -c -e "^\+$" ${FILEDIR}_Assembled.fastq) >> Count.log

## MASKQUAL 

$RUN  /home/userlab/presto-0.5.0/bin/FilterSeq.py maskqual -s R1_assemble-fail_rc.fastq -q $QUAL_MASK --nproc $NPROC  >> $RUNLOG_R1
echo "READ1-MASKQUALITY9>" $(grep -c '\+$' R1_assemble-fail_rc_maskqual-pass.fastq) >> Count.log

################ Remove sequences with many Ns #################
echo "10 Filter high-N sequences:" 

## FILTERSEQ R1 ONLY
$RUN  /home/userlab/presto-0.5.0/bin/FilterSeq.py missing -s R1_assemble-fail_rc_maskqual-pass.fastq  -n $N_MISS --inner --failed --fasta --nproc $NPROC  >> $RUNLOG_R1
R1=${R1}_missing-pass

## FILTERSEQ Assembled
$RUN  /home/userlab/presto-0.5.0/bin/FilterSeq.py missing -s ${FILEDIR}_Assembled_maskqual-pass.fastq -n $N_MISS --inner --failed --fasta --nproc $NPROC  >> $RUNLOG

$RUN  /home/userlab/presto-0.5.0/bin/ParseHeaders.py collapse -s ${FILEDIR}_Assembled_maskqual-pass_missing-pass.fasta -f CONSCOUNT --act min 
$RUN  /home/userlab/presto-0.5.0/bin/ParseHeaders.py collapse -s R1_assemble-fail_rc_maskqual-pass_missing-pass.fasta -f CONSCOUNT --act min 

echo "READ12-FILTERED_AMBIG11>" $(grep -c '^>' ${FILEDIR}_Assembled_maskqual-pass_missing-pass_reheader.fasta) >> Count.log
echo "READ1-FILTERED_AMBIG11>" $(grep -c '^>' R1_assemble-fail_rc_maskqual-pass_missing-pass.fasta) >> Count.log


################# Remove duplicate sequences #################
echo "14: collapseSequences" 

$RUN /home/userlab/presto-0.5.0/bin/CollapseSeq.py -s ${FILEDIR}_Assembled_maskqual-pass_missing-pass_reheader.fasta  -n $N_COLLAPSE --inner --uf PRCONS  --cf CONSCOUNT --act sum  >> $RUNLOG
$RUN /home/userlab/presto-0.5.0/bin/CollapseSeq.py -s R1_assemble-fail_rc_maskqual-pass_missing-pass_reheader.fasta  -n $N_COLLAPSE --inner --uf PRCONS  --cf CONSCOUNT --act sum  >> $RUNLOG_R1
# Jason has a speedup here but I haven't implemented it yet. 

/home/userlab/presto-0.5.0/bin/ParseHeaders.py table -s R1*_collapse-unique.fasta -f ID PRCONS CONSCOUNT DUPCOUNT --outname "Unique_R1"
/home/userlab/presto-0.5.0/bin/ParseHeaders.py table -s ${FILEDIR}*_collapse-unique.fasta -f ID PRCONS CONSCOUNT DUPCOUNT --outname "Unique_R12"

echo "READ12-UNIQUE_ASSEMBLED>" $(grep ">" -c ${FILEDIR}_Assembled_maskqual-pass_missing-pass_reheader_collapse-unique.fasta) >> Count.log
echo "READ1-UNIQUE_ASSEMBLE-FAIL>" $(grep ">" -c R1_assemble-fail_rc_maskqual-pass_missing-pass_reheader_collapse-unique.fasta) >> Count.log



mv ${FILEDIR}_Assembled_maskqual-pass_missing-pass_reheader.fasta Assembled_notCollapsed.fasta
mv R1_assemble-fail_rc_maskqual-pass_missing-pass_reheader.fasta R1_assemble-fail_notCollapsed.fasta


mv ${FILEDIR}_Assembled_maskqual-pass_missing-pass_reheader_collapse-unique.fasta Assembled_Collapsed.fasta
mv R1_assemble-fail_rc_maskqual-pass_missing-pass_reheader_collapse-unique.fasta R1_assemble-fail_Collapsed.fasta
############################################
gzip -f R[12]_*.fastq



#############################################

cd -
