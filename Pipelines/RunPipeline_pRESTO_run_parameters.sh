#!/bin/bash
#parameters
#check commit team
#check branch commit
RUNTIME="Runtime.log"
RUN="/usr/bin/time -o $RUNTIME -a -f '%C\t%E\t%P\t%Mkb' nice -n19"
NPROC=16			#The number of simultaneous computational processes to execute
RUNLOG="Pipeline.log"		#Log Assembled files
RUNLOG_R1="Pipeline_R1.log"	#Log R1 files
PARAMLOG="param.log"

#Paths to addional scripts and databases
MUSCLE=/usr/local/bin/muscle3.8.31_i86linux64		#MUSCLE aligner
USEARCH=/home/userlab/usearch8.0.1623_i86linux32	 	 #USEARCH Path
REF_DB=/media/Data/Kidney_supp/human_gl_IGHV.fasta		 	 #Reference Database for IGHV/TRBV
PAIRAWK_BC=/media/Data/Kidney_supp/scripts/pairawk_bc.sh	 #Pair seqs and transfer barcodes between pairs 
PAIRAWK_PRCONS=/media/Data/Kidney_supp/scripts/pairawk_prcons.sh #Pair seqs after building consensus
PAIRAWK_ASS_FAIL=/media/Data/Kidney_supp/scripts/pairawk_assembleFail_new.sh #Get the failed to assemble R1 reads

#READ 1
PRCONSR1=0.6			#Read 1: primer frequency required to assign a consensus primer- BuildConsensus
R1_PRIMER_FILE=/media/Data/Kidney_supp/Primers/Primers.IGHC_R1.no_ambig.fasta		#path of read 1 primer
PR1ST=0				#Primer Read 1 Start (might be different than 0 - UID)


#READ 2
PRCONSR2=0.6			#Read 2: primer frequency required to assign a consensus primer- BuildConsensus
R2_PRIMER_FILE=/media/Data/Kidney_supp/Primers/primers.V3_R2_TS-shift.fasta		#path of read 2 primer if NOT specified in the shell file
PR2ST=17			#Primer Read 2 Start

#FILE INFO
FILEPREFFIX=""			#file prefix	

#MASK PRIMERS
MAXERR=0.2			#maximium allowable error rate in MaskPrimers score (default: 0.2)
CHECK_COMP_PRIMERS='YES' 	# YES/NO
MP_CREGION_PRIMERS=/media/Data/Kidney_supp/CRegion.fasta
MP_CREGION_MAXLEN=100
MP_CREGION_MAXERR=0.4

#BUILD CONSENSUS
MAXDIV=0.1			#maxdiv calculation considers UID read group diversity before any factors effecting consensus generation (default: 0.2)

#FILTER SEQ
QUAL=5 				#Quality score threshold for initial FilterSeq quality (defualt:20)
QUAL_MASK=10			#Quality score thredhold for FilterSeq maskqual (default:20)
N_MISS=10			#Threshold for fraction of gap or N nucleotides in FilterSeq missing (default:10)

#COLLAPSE SEQ
N_COLLAPSE=10			#Maximum number of missing nucleotides to consider for collapsing sequences in CollapseSeq (default:0)

#ASSEMBLE PAIRS
MINLEN=8			#the minimum sequence length overlap for AssmeblePairs align
ALPHA=1.00E-005			#the significance threshold for AssemblePairs align
MAXERR_ASS=0.3			#Max error in AssemblePairs align
REV="tail" 			#Reverse complement - head/tail/both

